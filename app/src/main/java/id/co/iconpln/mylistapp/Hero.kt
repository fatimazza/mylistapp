package id.co.iconpln.mylistapp

data class Hero(
    var name: String = "",
    var desc: String = "",
    var photo: String = ""
)
